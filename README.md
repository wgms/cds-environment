# cds-environment

Defines the environment to be used by the Jupyter Notebooks in `cds-notebooks`. A separate environment repository ensures that the virtual machine is only rebuilt on changes to the environment, which speeds up opening a new session in [MyBinder](https://mybinder.org) (see [this forum post](https://discourse.jupyter.org/t/how-to-reduce-mybinder-org-repository-startup-time/4956)).

Two two repositories can then be launched together using a special link built using [this form](https://nbgitpuller.readthedocs.io/en/latest/link.html?tab=binder) and replacing `gh` (GitHub) with `gl` (GitLab).
